/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.daniel;

/**
 *
 * @author DanielPaz
 */
public class Pizzeria {
    public static void main(String[] args) {
        //03 Instansiar objetos 
        Pizza pizza1 = new Pizza();//Siempre seguir el paso a paso
        pizza1.getNombre();        //del codigo
        pizza1.setNombre("Española");
        System.out.println(pizza1.getNombre());
        
        Pizza pizza2 = new Pizza("Pepperoni","Familiar","Normal");
        System.out.println("Pizza 2: "+pizza2.toString());
        
        Pizza pizza3 = new Pizza("Salame","Mediana","Extra");
        System.out.println("");
        pizza3.preparar();
        pizza3.Calentar();
        System.out.println("");
        System.out.println("Pizza 3: "+pizza3.toString());
        System.out.println("Pizza 3: "+pizza3.getMasa());
        System.out.println("Pizza 3: "+pizza3.getNombre());
        System.out.println("Pizza 3: "+pizza3.getTamano());
        
        
        //Actualizar datos
        pizza1.setNombre("vegetariana");
        System.out.println("Pizza 1: "+pizza1.getNombre());
    }
}
