/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.daniel;

/**
 *
 * @author DanielPaz
 */
public class Pizza {
    //01 crear atributos 
    private String nombre, tamano, masa;
    
    //02 contruir metodos (customer)
    public void preparar(){
        System.out.println("Estamos preparando tu pizza");
    }
    public void Calentar(){
        System.out.println("Estamos calentando tu pizza");
    }
    //04 Crear constructor con y sin parametros

    public Pizza() {//Click derecho (insert code),(constructor),(generate).
    }

    public Pizza(String nombre, String tamano, String masa) {//(insert code),(constructor),
        this.nombre = nombre;                                //(Select all),(generate).
        this.tamano = tamano;
        this.masa = masa;
    }
    
    //05 Crear metodos setter y getter 

    public String getNombre() {//(insert code),(getter and setter),(Select all),(generate).
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }
    
    //06 Crear metodo toString

    @Override
    public String toString() {//(insert code),(toString),(Select all),(generate).
        return "Pizza{" + "Nombre=" + nombre + ", Tamaño=" + tamano + ", Masa=" + masa + '}';
    }
    
    
}


